import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SliderComponent } from './header/slider/slider.component';
import { FooterFormComponent } from './footer/footer-form/footer-form.component';
import { PageTemplateComponent } from './layout/page-template/page-template.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [LayoutComponent, HeaderComponent, FooterComponent, SliderComponent, FooterFormComponent, PageTemplateComponent],
  exports: [LayoutComponent]
  
})
export class UiModule { }
